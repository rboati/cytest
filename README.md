# README #

CyTest is a micro library of unit testing for C language.

Its main features/objectives are:

* automatic test discovery
* simple and easily portable
* setup and teardown of fixtures
* grouping of tests with tags

It is also:

* easily usable within IDEs like Visual Studio Express

It requires Python 2 or 3 for automatic test discovery.

TODO:

* documentation
* add makefile, Linux and OS X examples
* (?) use fork on unixes
* (?) add discovery implementations in other scripting languages (Ruby, Bash, JScript + MS Build, ...)