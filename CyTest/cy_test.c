#include "cy_test.h"

#if defined(_MSC_VER)

#pragma warning(push, 3)
#pragma warning(disable:4710) // warning C4710: 'XXX' : function not inlined
#pragma warning(disable:4711) // warning C4711: function 'XXX' selected for automatic inline expansion
#define NOGDICAPMASKS
#define NOVIRTUALKEYCODES
#define NOWINMESSAGES
#define NOWINSTYLES
#define NOSYSMETRICS
#define NOMENUS
#define NOICONS
#define NOKEYSTATES
#define NOSYSCOMMANDS
#define NORASTEROPS
#define NOSHOWWINDOW
#define OEMRESOURCE
#define NOATOM
#define NOCLIPBOARD
#define NOCOLOR
#define NOCTLMGR
#define NODRAWTEXT
#define NOGDI
#define NOKERNEL
#define NOUSER
#define NONLS
#define NOMB
#define NOMEMMGR
#define NOMETAFILE
#define NOMINMAX
#define NOMSG
#define NOOPENFILE
#define NOSCROLL
#define NOSERVICE
#define NOSOUND
#define NOTEXTMETRIC
#define NOWH
#define NOWINOFFSETS
#define NOCOMM
#define NOKANJI
#define NOHELP
#define NOPROFILER
#define NODEFERWINDOWPOS
#define NOMCX
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <strsafe.h> // Need to be included after tchar.h

void cy_test_log_va(char const* str, va_list va) {
	char buff[4096];
	StringCbVPrintfExA(buff, sizeof(buff), NULL, NULL, STRSAFE_NULL_ON_FAILURE | STRSAFE_IGNORE_NULLS , str, va);
	OutputDebugStringA(buff);
}

#pragma warning(pop)

#else

#include <stdio.h>

void cy_test_log_va(char const* str, va_list va) {
	vprintf(str, va);
	fflush(stdout);
}

#endif

void cy_test_log(char const* str, ...) {
	va_list va;
	va_start(va, str);
	cy_test_log_va(str, va);
	va_end(va);
}
