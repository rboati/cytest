#ifndef CY_TEST_H
#define CY_TEST_H

#include <stdarg.h>

#if !defined(FALSE)
#define FALSE 0
#endif

#if !defined(TRUE)
#define TRUE 1
#endif

#define CY_MACRO_STRING_(s)  # s
#define CY_MACRO_STRING(s)   CY_MACRO_STRING_(s)

#if defined(_MSC_VER)
#define CY_MACRO_BEGIN       __pragma(warning(push)) __pragma(warning(disable:4127 4548)) do{
#define CY_MACRO_END         }while(0) __pragma(warning(pop))
#else
#define CY_MACRO_BEGIN       do{
#define CY_MACRO_END         }while(0)
#endif

extern void cy_test_log(char const* str, ...);
extern void cy_test_log_va(char const* str, va_list va);
#define CY_TEST_LOG(msg, ...)         CY_MACRO_BEGIN cy_test_log(msg, __VA_ARGS__); CY_MACRO_END
#define CY_TEST_LOG_ERROR(msg, ...)   CY_MACRO_BEGIN cy_test_log("\n" __FILE__ "(" CY_MACRO_STRING(__LINE__) "): "); cy_test_log(msg, __VA_ARGS__); cy_test_log("\n"); CY_MACRO_END

extern int cy_test_result;
#define CY_TEST_TRUE(cond, ...)   CY_MACRO_BEGIN if (!(cond)) {CY_TEST_LOG_ERROR("ASSERT (" # cond ") " ## __VA_ARGS__); cy_test_result = FALSE; return;} CY_MACRO_END

//#include <setjmp.h>
//extern jmp_buf cy_test_jmp;
//#define CY_TEST_TRUE(cond, ...)   CY_MACRO_BEGIN if (!(cond)) {CY_TEST_LOG_ERROR("ASSERT (" # cond ") " ## __VA_ARGS__); longjmp(cy_test_jmp, 1);} CY_MACRO_END

#define CY_TEST_FALSE(cond, ...)  CY_TEST_TRUE(!(cond), __VA_ARGS__)
#define CY_TEST_EQ(a,b, ...)      CY_TEST_TRUE((a)==(b), __VA_ARGS__)
#define CY_TEST_NEQ(a,b, ...)     CY_TEST_TRUE((a)!=(b), __VA_ARGS__)

#define CY_TEST_SETUP(fixture_name)             void setup___ ## fixture_name ## (void)
#define CY_TEST_TEARDOWN(fixture_name)          void teardown___ ## fixture_name ## (void)
#define CY_TEST(fixture_name, test_name, ...)   void test___ ## fixture_name ## ___ ## test_name ## (void)


#endif