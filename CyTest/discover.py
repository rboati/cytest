import glob
import sys
import os
import fnmatch
import re
from collections import namedtuple
import logging


logging.basicConfig(level=logging.INFO, stream=sys.stderr, format="%(levelname)s: %(msg)s")
log = logging.getLogger()


class Config(object):
    def __init__(self):
        self.rules = []
        self.paths = []


RuleTuple = namedtuple("RuleTuple", "fixture, test, tags, flags")
TestTuple = namedtuple("TestTuple", "fixture, test, tags")


def read_config(config):
    try:
        with open("discover.config", "r") as f:
            re_comment = re.compile("#.*$")
            re_rule = re.compile(
                r"""^\s*(?P<exclude>-)?\s*(?P<fixture>[\w*]+)\s*,\s*(?P<test>[\w*]+)\s*,\s*(?P<tags>[\w*\s]+)$""")
            line_num = 0

            class ParsingState(object):
                READING_PATHS = 1
                READING_RULES = 2

                def __init__(self, cur):
                    self.cur = cur

            state = ParsingState(ParsingState.READING_RULES)

            def is_section():
                if line == "[Rules]":
                    state.cur = ParsingState.READING_RULES
                    return True
                if line == "[Paths]":
                    state.cur = ParsingState.READING_PATHS
                    return True
                return False

            for line in f:
                line_num += 1
                line = re.sub(re_comment, '', line)
                line = line.strip()
                if line == '':
                    continue

                if state.cur is ParsingState.READING_PATHS:
                    if is_section():
                        continue
                    for p in line.split(";"):
                        p = p.strip()
                        config.paths.append(os.path.normpath(p))
                    continue

                elif state.cur is ParsingState.READING_RULES:
                    if is_section():
                        continue
                    r = re_rule.match(line)
                    if not r:
                        raise Exception("""Error while parsing configuration file at:\n"""
                                        """    File "discover.config", line %i""" % line_num)
                    tags = set()
                    tags_txt = r.groupdict()['tags'].strip()
                    if tags_txt:
                        for tag in tags_txt.split():
                            tag = tag.strip()
                            if tag:
                                tags.add(tag)

                    flags = set()
                    exclude_flag = r.groupdict()['exclude']
                    if exclude_flag:
                        flags.add("-")
                    rule = RuleTuple(r.groupdict()['fixture'], r.groupdict()['test'], tags, flags)
                    config.rules.append(rule)
                    continue
    except IOError:
        del config.rules[:]
    if not config.rules:
        rule = RuleTuple("*", "*", set("*"), set())
        config.rules.append(rule)
    if not config.paths:
        config.paths.append(os.path.normpath("test_*.c"))

    for i in config.rules:
        log.debug(i)


def match_rule(test_tuple, rule):
    assert isinstance(test_tuple, TestTuple)
    assert isinstance(rule, RuleTuple)
    if not fnmatch.fnmatch(test_tuple.fixture, rule.fixture):
        return False
    if not fnmatch.fnmatch(test_tuple.test, rule.test):
        return False

    assert len(rule.tags) > 0

    for tag_rule in rule.tags:
        for tag in test_tuple.tags:
            if fnmatch.fnmatch(tag, tag_rule):
                break
        else:
            return False

    return True


def match_all_rules(test_tuple, rules):
    include_flag = False
    for rule in rules:
        assert isinstance(rule, RuleTuple)
        if '-' not in rule.flags:
            if match_rule(test_tuple, rule):
                include_flag = True
        else:
            if match_rule(test_tuple, rule):
                include_flag = False
    return include_flag


def get_outfile(filename):
    if filename == "-":
        return sys.stdout
    return open(filename, "w")


def check_init_data_for_conflicts(setups, teardowns, tests):
    res = True
    for fixture, filepos_list in setups.items():
        if len(filepos_list) > 1:
            pos_list = ("""    File "%s", line %s""" % (testfile, line_num) for testfile, line_num in filepos_list)
            pos_txt = "\n".join(pos_list)
            log.error("""Conflicting setup functions for fixture "%s" at:\n%s""" % (fixture, pos_txt))
            res = False
    for fixture, filepos_list in teardowns.items():
        if len(filepos_list) > 1:
            pos_list = ("""    File "%s", line %s""" % (testfile, line_num) for testfile, line_num in filepos_list)
            pos_txt = "\n".join(pos_list)
            log.error("""Conflicting teardown functions for fixture "%s" at:\n%s""" % (fixture, pos_txt))
            res = False
    for fixture, test_dict in tests.items():
        for test, filepos_list in test_dict.items():
            if len(filepos_list) > 1:
                pos_list = ("""    File "%s", line %s""" % (testfile, line_num) for testfile, line_num in filepos_list)
                pos_txt = "\n".join(pos_list)
                log.error("""Conflicting "%s" test functions for fixture "%s" at:\n%s""" % (test, fixture, pos_txt))
                res = False
    return res


def fill_init_data(config, execution_list, setups, teardowns, tests):
    file_set = set()
    re_setup = re.compile(r"\s*CY_TEST_SETUP\s*\(\s*(?P<fixture>\w*)\s*\)\s*")
    re_teardown = re.compile(r"\s*CY_TEST_TEARDOWN\s*\(\s*(?P<fixture>\w*)\s*\)\s*")
    re_test = re.compile(r"\s*CY_TEST\s*\(\s*(?P<fixture>\w*)\s*,\s*(?P<test>\w*)\s*(,\s*(?P<tags>.*?))?\)\s*")
    untagged_set = {"untagged"}
    for rule in config.rules:
        assert isinstance(rule, RuleTuple)
        for pat in config.paths:
            for testfile in glob.glob(pat):
                if testfile in file_set:
                    continue
                file_set.add(testfile)
                with open(testfile, "r") as f:
                    line_num = 0
                    for line in f:
                        line_num += 1
                        r = re_setup.match(line)
                        if r:
                            fixture = r.groupdict()['fixture']
                            filepos_list = setups.setdefault(fixture, [])
                            filepos_list.append((testfile, line_num))

                        r = re_teardown.match(line)
                        if r:
                            fixture = r.groupdict()['fixture']
                            filepos_list = teardowns.setdefault(fixture, [])
                            filepos_list.append((testfile, line_num))

                        r = re_test.match(line)
                        if r:
                            fixture = r.groupdict()['fixture']
                            test = r.groupdict()['test']
                            test_dict = tests.setdefault(fixture, dict())
                            filepos_list = test_dict.setdefault(test, [])
                            filepos_list.append((testfile, line_num))
                            tags_txt = r.groupdict()['tags'] or ""
                            tags_txt = tags_txt.replace(",", " ")
                            tags_txt = tags_txt.strip()
                            if tags_txt:
                                tags = set()
                                tags.update(tags_txt.split())
                                tags.discard("untagged")
                            else:
                                tags = untagged_set
                            test_tuple = TestTuple(fixture, test, tags)
                            execution_list.append(test_tuple)


# noinspection PyUnusedLocal
def write_c_header_longjmp(config, execution_list, f, setups, teardowns, tests):
    f.write("""\
/* This is a generated file. */

#include "cy_test.h"

#include <setjmp.h>
jmp_buf cy_test_jmp;

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable:4711) // warning C4711: function 'XXX' selected for automatic inline expansion
#endif

#define CY_RUN_TEST(fixture_name, test_name)   run_test___ ## fixture_name ## ___ ## test_name ## ()
extern void run_all_tests(int* total, int* failed);

""")
    for fixture, v in tests.items():
        for test in v:
            f.write("""\
extern int run_test___%(fixture)s___%(test)s(void);
""" % vars())
    for k in setups:
        f.write("""\
extern void setup___%s(void);
""" % k)
    for k in teardowns:
        f.write("""\
extern void teardown___%s(void);
""" % k)
    for k, v in tests.items():
        for i in v:
            f.write("""\
extern void test___%s___%s(void);
""" % (k, i))
    for fixture, v in tests.items():
        for test in v:
            f.write("""
int run_test___%(fixture)s___%(test)s(void) {
    int result = TRUE;
""" % vars())
            if fixture in setups:
                f.write("""\
    setup___%(fixture)s();
""" % vars())
            f.write("""\
    if (setjmp(cy_test_jmp) == 0) {
        test___%(fixture)s___%(test)s();
    } else {
        result = FALSE;
    }
""" % vars())
            if fixture in teardowns:
                f.write("""\
    teardown___%(fixture)s();
""" % vars())
            f.write("""\
    return result;
}
""" % vars())
    f.write("""
void run_all_tests(int* total, int* failed) {
    (void)total;
    (void)failed;
""")
    for test_tuple in execution_list:
        if not match_all_rules(test_tuple, config.rules):
            log.debug("- %s" % (test_tuple,))
            continue
        log.debug("+ %s" % (test_tuple,))
        fixture, test, tags = test_tuple
        f.write("""\
    ++(*total);
    if (run_test___%(fixture)s___%(test)s()) {
        CY_TEST_LOG("test %(fixture)s/%(test)s: ok\\n");
    } else {
        CY_TEST_LOG("test %(fixture)s/%(test)s: FAIL\\n");
        ++(*failed);
    }
""" % vars())
    f.write("""\
}

#if defined(_MSC_VER)
#pragma warning(pop)
#endif

""")


# noinspection PyUnusedLocal
def write_c_header(config, execution_list, f, setups, teardowns, tests):
    f.write("""\
/* This is a generated file. */

#include "cy_test.h"

int cy_test_result;

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable:4711) // warning C4711: function 'XXX' selected for automatic inline expansion
#endif

#define CY_RUN_TEST(fixture_name, test_name)   run_test___ ## fixture_name ## ___ ## test_name ## ()
extern void run_all_tests(int* total, int* failed);

""")
    for fixture, v in tests.items():
        for test in v:
            f.write("""\
extern int run_test___%(fixture)s___%(test)s(void);
""" % vars())
    for k in setups:
        f.write("""\
extern void setup___%s(void);
""" % k)
    for k in teardowns:
        f.write("""\
extern void teardown___%s(void);
""" % k)
    for k, v in tests.items():
        for i in v:
            f.write("""\
extern void test___%s___%s(void);
""" % (k, i))
    for fixture, v in tests.items():
        for test in v:
            f.write("""
int run_test___%(fixture)s___%(test)s(void) {
""" % vars())
            if fixture in setups:
                f.write("""\
    setup___%(fixture)s();
""" % vars())
            f.write("""\
    cy_test_result = TRUE;
    test___%(fixture)s___%(test)s();
""" % vars())
            if fixture in teardowns:
                f.write("""\
    teardown___%(fixture)s();
""" % vars())
            f.write("""\
    return cy_test_result;
}
""" % vars())
    f.write("""
void run_all_tests(int* total, int* failed) {
    (void)total;
    (void)failed;
""")
    for test_tuple in execution_list:
        if not match_all_rules(test_tuple, config.rules):
            log.debug("- %s" % (test_tuple,))
            continue
        log.debug("+ %s" % (test_tuple,))
        fixture, test, tags = test_tuple
        f.write("""\
    ++(*total);
    if (run_test___%(fixture)s___%(test)s()) {
        CY_TEST_LOG("test %(fixture)s/%(test)s: ok\\n");
    } else {
        CY_TEST_LOG("test %(fixture)s/%(test)s: FAIL\\n");
        ++(*failed);
    }
""" % vars())
    f.write("""\
}

#if defined(_MSC_VER)
#pragma warning(pop)
#endif

""")


def main():
    config = Config()

    try:
        read_config(config)
    except Exception as e:
        log.error(e.message)
        exit(-1)

    filename = "-"

    if len(sys.argv) > 1:
        filename = sys.argv[1]

    setups = {}
    teardowns = {}
    tests = {}
    execution_list = []

    fill_init_data(config, execution_list, setups, teardowns, tests)

    if not check_init_data_for_conflicts(setups, teardowns, tests):
        exit(-2)

    with get_outfile(filename) as f:
        #write_c_header_longjmp(config, execution_list, f, setups, teardowns, tests)
        write_c_header(config, execution_list, f, setups, teardowns, tests)


if __name__ == "__main__":
    main()
