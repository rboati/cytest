#include "cy_test.h" // CY_TEST_LOG(...)
#include "cy_test.generated.h" // run_all_tests(...)


int main(int argc, char* argv[]) {
	(void)argc;
	(void)argv;

	int total = 0;
	int failed = 0;
	run_all_tests(&total, &failed);
	CY_TEST_LOG("\nExecuted %i tests: %i successful and %i failed.\n", total, total-failed, failed);

	return failed;
}

